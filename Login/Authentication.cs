﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Login
{
    [Activity(Label = "Login", MainLauncher = true, Icon = "@mipmap/icon")]
    public class Authentication : Activity
    {
        

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Authentication);

            // Get button and Edit Text from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.loginButton);
            EditText idText = FindViewById<EditText>(Resource.Id.IdEditText);
            EditText passwordText = FindViewById<EditText>(Resource.Id.passwordEditText);

            button.Click += delegate {

                //declaring static username and password
                 var id = "Abdur";
                 var password = "123456";

                //switching to Main to start the process again
                StartActivity(typeof(MainActivity));

                //authenticating 
                if (idText.Text == id && passwordText.Text == password)
                {
                    //displaying toast
                    Toast.MakeText(this, "Login Successful", ToastLength.Long).Show();
                }
                else
                    Toast.MakeText(this, "Login Unsuccessful", ToastLength.Long).Show(); 
            };

        }
    }
}

